__all__ = ['gidisk', 'ring', 'ringrad', 'selfsim']

import sys
if sys.version_info[0] == 3:
    # Python 3 relative import rules
    from vader.test.gidisk import gidisk
    from vader.test.ring import ring
    from vader.test.ringrad import ringrad
    from vader.test.selfsim import selfsim
elif sys.version_info[0] == 2:
    # Python 2 relative import rules
    from gidisk import gidisk
    from ring import ring
    from ringrad import ringrad
    from selfsim import selfsim
